package com.example.sanchishugo_projecte3.ui.screens

import android.content.Context
import android.content.Intent
import androidx.compose.runtime.Composable
import androidx.lifecycle.ViewModel
import com.example.sanchishugo_projecte3.DetailActivity
import com.example.sanchishugo_projecte3.MainActivity
import com.example.sanchishugo_projecte3.data.apiservice.model.UsersDTO
import com.example.sanchishugo_projecte3.ui.model.CatDogUiBreedModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow


class LoginViewModel : ViewModel() {
    private var _cdUiState = MutableStateFlow<UsersDTO>(UsersDTO(listOf(""), listOf("")))
    public val CdUiState: StateFlow<UsersDTO> = _cdUiState.asStateFlow()

    init{
loginLists()
    }
fun loginLists(){
    _cdUiState.value = UsersDTO(listOf("hugo","maria","marc"),listOf("hugo1234","maria1234","marc1234"))
}

}