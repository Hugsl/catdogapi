package com.example.sanchishugo_projecte3

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import com.example.sanchishugo_projecte3.ui.screens.CatDogDetailViewModel
import com.example.sanchishugo_projecte3.ui.screens.CatDogViewModel
import com.example.sanchishugo_projecte3.ui.screens.DetailScreen
import com.example.sanchishugo_projecte3.ui.screens.homeScreen
import com.example.sanchishugo_projecte3.ui.theme.Sanchishugo_projecte3Theme

class DetailActivity: ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Sanchishugo_projecte3Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Greetings(id=intent.getStringExtra("ID").toString())
                }
            }
        }

    }

}

@Composable
fun Greetings(viewModel: CatDogDetailViewModel = androidx.lifecycle.viewmodel.compose.viewModel(), id:String) {
    val context = LocalContext.current
    val a by viewModel.uiState.collectAsState()
    if(a.id == "aa"){
        viewModel.loadCatDogModel(
            id
        )
    }
    DetailScreen(CDB = a, context = context)
}
