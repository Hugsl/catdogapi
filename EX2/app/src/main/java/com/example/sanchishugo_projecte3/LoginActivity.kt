package com.example.sanchishugo_projecte3

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import com.example.sanchishugo_projecte3.ui.screens.LoginScreen
import com.example.sanchishugo_projecte3.ui.screens.LoginViewModel
import com.example.sanchishugo_projecte3.ui.theme.Sanchishugo_projecte3Theme
import androidx.lifecycle.viewmodel.compose.viewModel


class LoginActivity: ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Sanchishugo_projecte3Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                   IdentifyYourself()
                }
            }
        }
    }
}

@Composable
fun IdentifyYourself(viewModel: LoginViewModel = viewModel()) {
val context = LocalContext.current
    val a by viewModel.CdUiState.collectAsState()
    LoginScreen(LVM = a, context = context, modifier = Modifier)
    
    
}

