package com.example.sanchishugo_projecte3.ui.screens

import androidx.compose.runtime.Composable

import android.content.Context
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.sanchishugo_projecte3.ui.model.CatDogUiBreedModel

@Composable

fun DetailScreen(CDB: CatDogUiBreedModel, modifier: Modifier =Modifier, context: Context){
DetailScreenValues(CDB = CDB, context = context)
}


@Composable
fun DetailScreenValues(CDB: CatDogUiBreedModel, modifier: Modifier =Modifier, context: Context){



    LazyColumn(verticalArrangement = Arrangement.spacedBy(4.dp), contentPadding = PaddingValues(8.dp)) {
        item { CatDogDetailCard(CDB, modifier, context) }
    }
}

@Composable
fun CatDogDetailCard(CDB: CatDogUiBreedModel, modifier: Modifier =Modifier, context: Context){

    val expanded by remember {  mutableStateOf(false)    }

    Card(modifier = Modifier
        .padding(16.dp)
        .fillMaxHeight()) {
        val a = CDB.CDI
        Column(modifier = Modifier.fillMaxHeight()) {

            AsyncImage(model = a.url, contentDescription = a.url)
            Text(text = CDB.name.replaceFirst(CDB.id[0],CDB.id[0].uppercaseChar()), style = TextStyle(  fontSize = 30.sp , textAlign = TextAlign.Center))
            Text(text = "Alternative names: "+CDB.altNames)
            Text(text = "Measure in meters: "  + CDB.weight[0] )
            Text(text = "Measure in inches: "  + CDB.weight[1] )
            Text(text = "Origin: " + CDB.origin)
            Text(text = "Description: "+CDB.description.toString())
            Text(text = "Temperament: " + CDB.temperament)
            Text(text = "Wikipedia URL: " + CDB.wikipediaUrl)

       }
    }



}
