package com.example.sanchishugo_projecte3.data.apiservice


import com.example.sanchishugo_projecte3.data.apiservice.model.CatDogBreed
import com.example.sanchishugo_projecte3.data.apiservice.model.CatDogImageDto
import retrofit2.Retrofit
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

private const val BASE_URL = "https://api.thecatapi.com/v1/"
private val retrofit =
    Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create()).baseUrl(
        BASE_URL
    ).build()

interface CatDogApiService {

    @GET("./breeds")
    suspend fun getCatDogBreeds(): List<CatDogBreed>

    @GET("breeds/{breed}")
    suspend fun getCatDogBreed(@Path(value = "breed") breed: String): CatDogBreed

    @GET("images/search")
    suspend fun getCatDogImage(
        @Query(value = "breed_ids") id: String,
        @Query(value = "limit") limit: Int
    ): List<CatDogImageDto>

}

object CatDogApi {
    val retofitService: CatDogApiService by lazy {
        retrofit.create(CatDogApiService::class.java)
    }
}


