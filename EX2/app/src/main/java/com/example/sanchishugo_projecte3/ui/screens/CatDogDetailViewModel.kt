package com.example.sanchishugo_projecte3.ui.screens

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sanchishugo_projecte3.data.apiservice.CatDogApi
import com.example.sanchishugo_projecte3.ui.model.CatDogDtoUiModelMapper
import com.example.sanchishugo_projecte3.ui.model.CatDogUiBreedModel
import com.example.sanchishugo_projecte3.ui.model.CatDogUiImageModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class CatDogDetailViewModel() : ViewModel() {

    private val _uiState = MutableStateFlow<CatDogUiBreedModel>(
        CatDogUiBreedModel(
            weight = listOf(" "," "),
            "aa",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            1,
            1,
            "",
            5,
            5,
            1,
            0,
            100,
            0,
            10000000,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            "",
            0,
            "",
            CDI = CatDogUiImageModel("", "", 0, 0)
        )
    )

    val uiState: StateFlow<CatDogUiBreedModel> = _uiState.asStateFlow()

    fun loadCatDogModel(id: String) {

        viewModelScope.launch {
            _uiState.value =
                CatDogDtoUiModelMapper().mapBreed(CatDogApi.retofitService.getCatDogBreed(id))
        }

    }

}