package com.example.sanchishugo_projecte3.ui.model

import com.example.sanchishugo_projecte3.data.apiservice.CatDogApi
import com.example.sanchishugo_projecte3.data.apiservice.model.CatDogBreed
import com.example.sanchishugo_projecte3.data.apiservice.model.CatDogImageDto

class CatDogDtoUiModelMapper {

    suspend fun mapAll(listB: List<CatDogBreed>, listI: List<CatDogImageDto>): CatDogUiModel {
        return CatDogUiModel(mapBreed(listB))
    }

    suspend fun mapBreed(list: List<CatDogBreed>): List<CatDogUiBreedModel> {


        return mutableListOf<CatDogUiBreedModel>().apply {
            list.forEach() {

                add(
                    CatDogUiBreedModel(
                        mutableListOf(
                            it.weight.metric,
                            it.weight.imperial
                        ),
                        it.id,
                        it.name,
                        it.cfaUrl,
                        it.vetstreetUrl,
                        it.vcahospitalsUrl,
                        it.temperament,
                        it.origin,
                        it.countryCodes,
                        it.countryCode,
                        it.description,
                        it.lifeSpan,
                        it.indoor,
                        it.lap,
                        it.altNames,
                        it.adaptability,
                        it.affectionLevel,
                        it.childFriendly,
                        it.dogFriendly,
                        it.energyLevel,
                        it.grooming,
                        it.healthIssues,
                        it.intelligence,
                        it.sheddingLevel,
                        it.socialNeeds,
                        it.strangerFriendly,
                        it.vocalisation,
                        it.experimental,
                        it.hairless,
                        it.natural,
                        it.rare,
                        it.rex,
                        it.suppressedTail,
                        it.shortLegs,
                        it.wikipediaUrl,
                        it.hypoallergenic,
                        it.referenceImageId,
                        CDI = mapImage(it.id)

                    )
                )
            }

        }
    }



    suspend fun mapBreed(item:CatDogBreed):CatDogUiBreedModel{
        return CatDogUiBreedModel(

            mutableListOf(
            item.weight.metric,
            item.weight.imperial
        ),
            item.id,
            item.name,
            item.cfaUrl,
            item.vetstreetUrl,
            item.vcahospitalsUrl,
            item.temperament,
            item.origin,
            item.countryCodes,
            item.countryCode,
            item.description,
            item.lifeSpan,
            item.indoor,
            item.lap,
            item.altNames,
            item.adaptability,
            item.affectionLevel,
            item.childFriendly,
            item.dogFriendly,
            item.energyLevel,
            item.grooming,
            item.healthIssues,
            item.intelligence,
            item.sheddingLevel,
            item.socialNeeds,
            item.strangerFriendly,
            item.vocalisation,
            item.experimental,
            item.hairless,
            item.natural,
            item.rare,
            item.rex,
            item.suppressedTail,
            item.shortLegs,
            item.wikipediaUrl,
            item.hypoallergenic,
            item.referenceImageId,
            CDI = mapImage(item.id)
        )

    }



    suspend fun mapImage(imageid: String?): CatDogUiImageModel {
        if (imageid != null) {
            val a = CatDogApi.retofitService.getCatDogImage(
                imageid.toString(),
                2
            )
            if(a.size>0) return CatDogUiImageModel(a[0].id, a[0].url, a[0].height, a[0].width)
        }

    return  CatDogUiImageModel("ERROR", "ERROR", 0,0)
    }}