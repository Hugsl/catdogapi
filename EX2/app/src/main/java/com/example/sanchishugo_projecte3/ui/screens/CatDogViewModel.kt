package com.example.sanchishugo_projecte3.ui.screens

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sanchishugo_projecte3.data.apiservice.CatDogApi
import com.example.sanchishugo_projecte3.ui.model.CatDogDtoUiModelMapper
import com.example.sanchishugo_projecte3.ui.model.CatDogUiBreedModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class CatDogViewModel:ViewModel() {

    private var _cdUiState = MutableStateFlow<List<CatDogUiBreedModel>>(emptyList())
    public val CdUiState : StateFlow<List<CatDogUiBreedModel>> = _cdUiState.asStateFlow()

    init {
        getCDPhotos()
    }

    fun getCDPhotos() {
        viewModelScope.launch {
            _cdUiState.value = CatDogDtoUiModelMapper().mapBreed(CatDogApi.retofitService.getCatDogBreeds())
        }
    }
}

