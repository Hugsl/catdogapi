//@file:OptIn(ExperimentalFoundationApi::class)

package com.example.sanchishugo_projecte3.ui.screens
import android.content.Context

import android.content.Intent
import android.text.style.LineHeightSpan
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.unit.dp
import com.example.sanchishugo_projecte3.ui.model.CatDogUiBreedModel
import com.example.sanchishugo_projecte3.ui.model.CatDogUiModel
import  androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.capitalize
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.toUpperCase
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.compose.rememberAsyncImagePainter
import com.example.sanchishugo_projecte3.DetailActivity

@Composable
fun homeScreen(CDUS:List<CatDogUiBreedModel>,modifier: Modifier =Modifier,context: Context){

    ResultsScreen(CDUS = CDUS, modifier =modifier ,context)

}
@Composable
fun ResultsScreen(CDUS: List<CatDogUiBreedModel>, modifier: Modifier = Modifier,context: Context){

    LazyColumn(verticalArrangement = Arrangement.spacedBy(4.dp), contentPadding = PaddingValues(8.dp)){
       items(CDUS) {  item:CatDogUiBreedModel -> CatDogCard(item,modifier,context)}
    }

}

@Composable
fun CatDogCard(CDBU : CatDogUiBreedModel,modifier: Modifier = Modifier,context: Context  ){

    val expanded by remember {  mutableStateOf(false)    }

    Card(modifier = Modifier
        .padding(16.dp)
        .fillMaxSize()) {
        val a = CDBU.CDI



Row() {
    Column(modifier = Modifier.fillMaxHeight()) {
        AsyncImage(model = a.url, contentDescription = "")
        Text(text = CDBU.name.replaceFirst(CDBU.id[0],CDBU.id[0].uppercaseChar()), style = TextStyle(  fontSize = 30.sp , textAlign = TextAlign.Center)



        )


        TextButton(onClick = {

            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("ID",CDBU.id)
            context.startActivity(intent)

        }) {

            Text(text = "Watch more")
        }
}


        }


    }
}

