package com.example.sanchishugo_projecte3.ui.screens

import android.content.Context
import android.content.Intent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Lock
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.TextFieldValue
import com.example.sanchishugo_projecte3.MainActivity
import com.example.sanchishugo_projecte3.data.apiservice.model.UsersDTO


@Composable
fun LoginScreen(LVM:UsersDTO,modifier: Modifier,context:Context) {

    var Username = remember { mutableStateOf(TextFieldValue()) }
    var Password = remember { mutableStateOf(TextFieldValue()) }
   Column(modifier = modifier.fillMaxSize()) {
       TextField(
           modifier = Modifier.fillMaxWidth(0.9f),
           colors = TextFieldDefaults.textFieldColors(
               textColor = Color.White,
               focusedIndicatorColor = Color.White,
               focusedLabelColor = Color.White
           ),
           value = Username.value,
           onValueChange = { Username.value = it },
           label = { Text("Username") }, leadingIcon = {
               Icon(
                   Icons.Filled.Email,
                   "contentDescription",
                   modifier = Modifier.clickable {})
           }

       )

       TextField(
           modifier = Modifier.fillMaxWidth(0.9f),
           colors = TextFieldDefaults.textFieldColors(
               textColor = Color.White,
               focusedIndicatorColor = Color.White,
               focusedLabelColor = Color.White,
           ),
           value = Password.value,
           onValueChange = { Password.value= it },
           label = { Text("Password") },
           leadingIcon = {
               Icon(
                   Icons.Filled.Lock,
                   "contentDescription",
                   modifier = Modifier.clickable {})
           }
       )
       Button(onClick = { checkPass(Username.value.text,Password.value.text,context,LVM)}) {


           Text(text = "Log IN!")
       }


   }






     
}

fun checkPass(uN: String, pW: String, context: Context,LVM: UsersDTO) {
    for (i in 0 until LVM.id.count()) {

        if (uN == LVM.id[i] && pW == LVM.pass[i]) {

            val intent = Intent(context, MainActivity::class.java)
            context.startActivity(intent)

        }

    }


}


